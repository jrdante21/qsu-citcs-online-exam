<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AdminsController as AdminsAPI;
use App\Http\Controllers\API\AdminsUserController as AdminsUserAPI;
use App\Http\Controllers\API\QuestionsController as QuestionsAPI;
use App\Http\Controllers\API\CoursesController as CoursesAPI;
use App\Http\Controllers\API\SubjectsController as SubjectsAPI;
use App\Http\Controllers\API\SchoolsController as SchoolsAPI;
use App\Http\Controllers\API\StudentsController as StudentsAPI;
use App\Http\Controllers\API\StudentsCountController as StudentsCountAPI;
use App\Http\Controllers\API\ExamTimelineController as ExamTimelineAPI;
use App\Http\Controllers\Admin\SpaController as AdminSpa;
use App\Http\Controllers\Admin\AuthController as AdminAuth;
use App\Http\Controllers\Admin\PDFController as PDF;
use App\Http\Controllers\Admin\ResetPasswordController as ResetPassword;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\StudentsExamController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Registration
Route::get('/', [RegistrationController::class, 'index'])->name('registration');
Route::post('/register', [RegistrationController::class, 'store']);

// Examination
Route::get('/students-exam/quit', [StudentsExamController::class, 'quit']);
Route::resource('students-exam', StudentsExamController::class)->names(['index' => 'students-exam']);

// Public API
Route::get('/search-school', [SchoolsAPI::class, 'public']);

// Admin
Route::prefix('administrator')->name('admin.')->group(function(){
    // Login & Logout
    Route::get('/', [AdminAuth::class, 'index'])->name('login');
    Route::get('/logout', [AdminAuth::class, 'logout'])->name('logout');
    Route::post('/login', [AdminAuth::class, 'login']);

    // Reset Password
    Route::get('/reset-password/{any?}', [ResetPassword::class, 'index'])->where('any','.*');
    Route::post('/reset-password', [ResetPassword::class, 'resetPassword']);
    
    // Panel
    Route::middleware('admin-auth')->group(function(){
        Route::get('/panel/{any?}', AdminSpa::class)->where('any','.*')->name('panel');
        Route::get('/questionnaire/{any?}', AdminSpa::class)->where('any','.*');
        Route::get('/pdf/{uuid}/{type?}', PDF::class);
    });

    // API
    Route::middleware('admin-auth')->prefix('api')->group(function () {
        Route::get('/schools', [SchoolsAPI::class, 'admin']);
        Route::get('/exam-timeline', ExamTimelineAPI::class);
        Route::get('/students-count', StudentsCountAPI::class);
        Route::put('/subjects/publish/{id}', [SubjectsAPI::class, 'publish']);
        Route::apiResource('questions', QuestionsAPI::class);
        Route::apiResource('courses', CoursesAPI::class);
        Route::apiResource('subjects', SubjectsAPI::class);
        Route::apiResource('students', StudentsAPI::class);
        Route::apiResource('admins', AdminsAPI::class);

        Route::post('/change-password', [AdminsUserAPI::class, 'changePassword']);
        Route::post('/update-profile/{admin}', [AdminsUserAPI::class, 'updateProfile']);
    });
});