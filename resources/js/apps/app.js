import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import { create,
    NButton, NCard, NEllipsis, NEmpty, NForm, NFormItem, NInput, NInputGroup, NInputGroupLabel, NInputNumber,
    NLayout, NLayoutContent, NLayoutFooter, NLayoutHeader, NLayoutSider, NMenu, NMessageProvider, NModal,
    NRadio, NRadioGroup, NDialogProvider, NProgress, NPagination, NPopover, NDatePicker, NSelect, NSwitch,
    NCheckbox, NCheckboxGroup, NTag, NTable, NSpin
} from 'naive-ui'
import App from '~/components/App.vue'
import Registration from '~/components/students/Registration.vue'
import Students from '~/components/students/Students.vue'
import StudentsExam from '~/components/students/StudentsExam.vue'

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: Registration
        },{
            path: '/students-exam',
            component: Students
        },{
            path: '/students-exam/:uuid',
            component: StudentsExam
        }
    ]
})

const app = createApp(App)
const naive = create({
    components: [
        NButton, NCard, NEllipsis, NEmpty, NForm, NFormItem, NInput, NInputGroup, NInputGroupLabel, NInputNumber,
        NLayout, NLayoutContent, NLayoutFooter, NLayoutHeader, NLayoutSider, NMenu, NMessageProvider, NModal,
        NRadio, NRadioGroup, NDialogProvider, NProgress, NPagination, NPopover, NDatePicker, NSelect, NSwitch,
        NCheckbox, NCheckboxGroup, NTag, NTable, NSpin
    ]
})

app.use(naive)
app.use(router)
app.mount('#app')