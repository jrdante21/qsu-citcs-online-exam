import { createApp } from 'vue'
import { createStore } from 'vuex'
import { createRouter, createWebHistory } from 'vue-router'
import { create,
    NButton, NCard, NEllipsis, NEmpty, NForm, NFormItem, NInput, NInputGroup, NInputGroupLabel, NInputNumber,
    NLayout, NLayoutContent, NLayoutFooter, NLayoutHeader, NLayoutSider, NMenu, NMessageProvider, NModal,
    NRadio, NRadioGroup, NSwitch, NTable, NTimePicker, NTag, NDialogProvider, NProgress, NPagination, NPopover, NSelect
} from 'naive-ui'

// Router
import App from '~/components/App.vue'

import AppNotFound from '~/components/admin/AppNotFound.vue'
import LoginRouter from '~/components/admin/Login.vue'
import AdminRouter from '~/components/admin/App.vue'
import AdminsRouter from '~/components/admin/Admins.vue'
import AccountRouter from '~/components/admin/Account.vue'
import HomeRouter from '~/components/admin/Home.vue'
import CoursesRouter from '~/components/admin/Courses.vue'
import StudentsRouter from '~/components/admin/Students.vue'
import SchoolsRouter from '~/components/admin/Schools.vue'
import ExamTimelineRouter from '~/components/admin/ExamTimeline.vue'
import QuestionBankRouter from '~/components/admin/QuestionBank.vue'
import QuestionnaireRouter from '~/components/admin/Questionnaire.vue'
import QuestionnaireEditorRouter from '~/components/admin/QuestionnaireEditor.vue'
import QuestionnairePreviewRouter from '~/components/admin/QuestionnairePreview.vue'
import ResetPasswordRoute from '~/components/admin/ResetPassword.vue'
const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/administrator',
            component: LoginRouter,
            name: 'login'
        },{
            path: '/administrator/panel',
            component: AdminRouter,
            children: [
                {
                    path: '/:pathMatch(.*)*',
                    name: 'panel-not-found',
                    component: AppNotFound,
                },{
                    path: '',
                    name: 'home',
                    component: HomeRouter
                },{
                    path: 'courses/:uuid',
                    name: 'courses',
                    component: CoursesRouter,
                    props: true
                },{
                    path: 'students/:uuid?',
                    name: 'students',
                    component: StudentsRouter,
                    props: true
                },{
                    path: 'question-bank',
                    name: 'question-bank',
                    component: QuestionBankRouter
                },{
                    path: 'admins',
                    name: 'admins',
                    component: AdminsRouter,
                    beforeEnter: (to) => {
                        const params = to.path.substring(1).split('/')
                        if (window.$admin.is_super <= 0) return {
                            name: 'panel-not-found',
                            params: { pathMatch: params.filter(e => !['administrator','panel'].includes(e)) },
                            query: to.query,
                            hash: to.hash,
                          }
                    }
                },{
                    path: 'schools',
                    name: 'schools',
                    component: SchoolsRouter
                },{
                    path: 'exam-timeline',
                    name: 'exam-timeline',
                    component: ExamTimelineRouter
                },{
                    path: 'account',
                    name: 'account',
                    component: AccountRouter
                }
            ]
        },{
            path: '/administrator/questionnaire/:uuid',
            component: QuestionnaireRouter,
            props: true,
            children: [
                {
                    path: '',
                    name: 'questionnaire-editor',
                    component: QuestionnaireEditorRouter
                },{
                    path: 'preview',
                    name: 'questionnaire-preview',
                    component: QuestionnairePreviewRouter
                }
            ]
        },{
            path: '/administrator/reset-password',
            component: ResetPasswordRoute,
            name: 'reset-password'
        }
    ]
})
router.beforeEach((to) => {
    const name = to.name.split('-').map(e => {
        return e.charAt(0).toUpperCase() + e.slice(1)
    })
    document.title = 'Administrator | '+name.join(' ')
})

const naive = create({
    components: [
        NLayout, NLayoutSider, NLayoutHeader, NLayoutContent, NLayoutFooter,
        NMenu, NEmpty, NTable, NButton, NModal, NForm, NFormItem, NTimePicker, NInputNumber,
        NInputGroup, NInputGroupLabel, NInput, NSwitch, NRadio, NRadioGroup, NSelect,
        NCard, NEllipsis, NMessageProvider, NTag, NDialogProvider, NProgress, NPagination, NPopover,
    ]
})

const store = createStore({
    state() {
        return {
            courses: [],
            admin: window.$admin
        }
    },
    mutations: {
        setCourses(state, data) {
            state.courses = data
        },
        setAdmin(state, data) {
            state.admin = data
        }
    }
})

const app = createApp(App)
app.use(router)
app.use(naive)
app.use(store)
app.mount('#app')