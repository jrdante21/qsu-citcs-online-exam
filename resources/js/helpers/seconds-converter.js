const secToHours = (seconds, toString=false) => {
    const h = Math.floor(seconds / 3600)
    const m = Math.floor(seconds / 60 % 60)
    const s = Math.floor(seconds % 60)
    
    if (!toString) return { h, m, s }

    const time = [h,m,s].map(e => e <= 9 ? '0'+e : e)
    return time.join(':')
}

const secToTime = (seconds=null) => {
    if (!seconds) return null

    const date = new Date()
    const { h, m, s } = secToHours(seconds)
    date.setHours(h,m,s)
    return date.getTime()
}

const timeToSec = (time=null) => {
    if (!time) return null

    const date = new Date(time)
    return Number(date.getHours() * 3600) + Number(date.getMinutes() * 60) + Number(date.getSeconds())
}

export { secToHours, secToTime, timeToSec }