const computedErrors = (errors) => {
    const feedback = {}
    const status = {}
    Object.keys(errors).forEach(e => {
        if (errors[e]) {
            status[e] = 'error'
            feedback[e] = errors[e].join('\n')
        }
    })

    return { feedback, status }
}

export { computedErrors }