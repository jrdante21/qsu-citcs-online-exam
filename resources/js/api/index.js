import axios from 'axios'

const API = axios.create({
    baseURL: '/administrator/api'
})

API.interceptors.response.use(r => { return r }, error => {
    const { data } = error.response
    switch (error.response.status) {
        case 401:
            location.reload()
            break;
    
        case 403:
            const msg = data != '' ? data : "You can't do this action."
            window.$messageAlert.error(msg, "Unauthorized action")
            break;

        case 500:
            window.$messageAlert.error("Try again later.", "Something went wrong")
            break;
    }

    return Promise.reject(error)
})

export default API
