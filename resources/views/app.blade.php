@extends('layouts.app')
@section('title', 'Students Validation Exam')
@section('content')
    <div id="app"></div>

    @isset($courses)
        <script> window.$courses = @json($courses) </script>
    @endisset

    @isset($student)
        <script> window.$student = @json($student) </script>
    @endisset

    @isset($subject)
        <script> window.$subject = @json($subject) </script>
    @endisset

    <script src="{{ mix('js/app.js') }}"></script>
@endsection