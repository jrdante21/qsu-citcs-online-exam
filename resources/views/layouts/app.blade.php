<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="icon" href="/images/logo/citcs.png">
    <meta name="naive-ui-style" />
    <title>@yield('title')</title>
    <style>
        td {
            background-color: transparent !important;
        }
    </style>
</head>
<body>
    @yield('content')
</body>
</html>