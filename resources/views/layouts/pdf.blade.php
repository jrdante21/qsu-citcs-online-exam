<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
</head>
<style type="text/css">
	body, html {
		font-family: sans-serif;
		font-size: 12px;
	}
	@page {
        size: auto;
        header: page-header;
	    footer: page-footer;
    }

    .header-line {
        border-bottom: 1px solid #ccc;
    }

	.my-table {
		border-collapse: collapse;
		font-family:sans-serif;
		width: 100%;
	}
	.my-table td, .my-table th{
		padding: 5px 3px;
	}
	.my-table th {
		text-align: left;
		background: #ccc;
	}

	.collapsing {
		width: 1px;
    	white-space: nowrap;
	}

	.celled.my-table td, .celled.my-table th{
		border: 1px solid #000;
	}
	.top-align.my-table td {
		vertical-align: top;
	}

</style>
<body>
	<table style="font-size:15px" width="100%">
		<tr>
			<td class="collapsing" style="border: 0px;"><img src="images/logo/citcs.png" style="width: 60px;"></td>
			<td style="border: 0px;" align="center"><h3>Quirino State College</h3> College of Information Technology and Computer Sciences</td>
			<td class="collapsing" style="border: 0px;"><img src="images/logo/qsu.png" style="width: 60px;"></td>
		</tr>
	</table>
    @yield('content')
</body>
</html>