@extends('layouts.app')
@section('title', 'Administrator')
@section('content')
    <div id="app"></div>
    @isset($admin)
        <script> window.$admin = @json($admin) </script>
    @endisset
    <script src="{{ mix('js/admin.js') }}"></script>
@endsection