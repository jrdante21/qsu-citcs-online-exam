@extends('layouts.pdf')
@section('title', $subject->students->fullname.' - '.$subject->subjects_name)
@section('content')
	<h3 class="header-line">Exam Information</h3>
	<table class="my-table" cellpadding="0" cellspacing="0">
		<tr>
			<td>Name: <b>{{ ucwords($subject->students->fullname) }}</b></td>
			<td align="right">Date / Time:
                <b>{{ date('M d, Y h:i:s', strtotime($subject->started_at)) }} - {{ date('h:i:s A', strtotime($subject->finished_at)) }}</b>
            </td>
		</tr>
		<tr>
			<td colspan="2">
				Subject: <b>{{ $subject->subjects_name }}</b> &emsp;
				Items: <b>{{ $subject->items_count }}</b> &emsp;
				Time Limit: <b>{{ $subject->time_str }}</b> &emsp;
			</td>
		</tr>
		<tr>
			<td colspan="2">
				Score: 
				Correct: <b>{{ $subject->results['correct'] }}</b> &emsp;
				Wrong: <b>{{ $subject->results['wrong'] }}</b> &emsp;
				Missed: <b>{{ $subject->results['missed'] }}</b> &emsp;
				Remarks: <b>{{ $subject->results['percent'] }}%</b> &emsp;
			</td>
		</tr>
	</table>

	<h3 class="header-line">Questionnaire</h3>
	<table class="top-align my-table" cellpadding="0" cellspacing="0">
        @php $num = 0; @endphp
        @foreach ($subject->questions_items as $item)
            @php
                if (empty($item['answerOwn'])) {
                    $icon = '<i style="color:orange">&#8722; Missed</i>';
                    
                } elseif ($item['answerOwn'] == $item['answer']) {
                    $icon = '<i style="color:green">&#10004; Correct</i>';

                } else {
                    $icon = '<i style="color:red">&#10006; Wrong</i>';
                }

                $num += 1;
            @endphp

            <tr>
                <td class="collapsing"><b>{{ $num }}.</b> &nbsp;</td>
                <td>{!! $icon !!} &nbsp; {{ $item['question'] }}</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <ul>
                        @foreach ($item['options'] as $option)
                            <li>
                                @if ($option['number'] == $item['answerOwn'] || $option['number'] == $item['answer'])
                                    <i style="color: gray">
                                        @if ($option['number'] == $item['answer'])
                                            Correct Answer: &nbsp;
                                        @else
                                            Your Answer: &nbsp;
                                        @endif
                                    </i>
                                @endif
                                {{ $option['text'] }}
                            </li>
                        @endforeach
                    </ul>
                </td>
            </tr>
            
        @endforeach
	</table>
@endsection