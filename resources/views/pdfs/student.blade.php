@extends('layouts.pdf')
@section('title', $student->fullname)
@section('content')
    <h3 class="header-line">Student Information</h3>
    <table class="my-table" cellpadding="0" cellspacing="0">
        <tr>
            <td>Name</td>
            <td><h3>{{ $student->fullname }}</h3></td>
        </tr>
        <tr>
            <td class="collapsing">Date Registered</td>
            <td><h3>{{ date('M d, Y h:i A', strtotime($student->created_at)) }}</h3></td>
        </tr>
        <tr>
            <td>Course</td>
            <td><h3>{{ $student->courses->title }}</h3></td>
        </tr>
        <tr>
            <td>School</td>
            <td><h3>{{ $student->schools->name }}</h3></td>
        </tr>
        <tr>
            <td>Birthdate</td>
            <td><h3>{{ date('M d, Y', strtotime($student->bday)) }}</h3></td>
        </tr>
        <tr>
            <td>Age</td>
            <td><h3>{{ $student->age }}</h3></td>
        </tr>
        <tr>
            <td>Gender</td>
            <td><h3>{{ $student->gender_str }}</h3></td>
        </tr>
    </table>

    <h3 class="header-line">Exam Results</h3>
    <table class="celled my-table">
        <thead>
            <tr>
                <th>Subject</th>
                <th>Remarks</th>
                <th>Date & Time</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($student->subjects as $item)
                <tr>
                    <td>
                        <div><b>{{ $item->subjects_title }}</b></div>
                        <div>
                            {{ $item->items_count }} Items &nbsp; | &nbsp;
                            Time: {{ $item->time_str }}
                        </div>
                    </td>
                    <td>
                        <div><b>{{ $item->results['percent'] }}%</b></div>
                        <div>
                            {{ $item->results['correct'] }} Correct &nbsp; | &nbsp;
                            {{ $item->results['wrong'] }} Wrong &nbsp; | &nbsp;
                            {{ $item->results['missed'] }} Missed
                        </div>
                    </td>
                    <td>
                        <div><b>{{ date('M d, Y', strtotime($item->started_at)) }}</b></div>
                        <div>
                            {{ date('h:i:s A', strtotime($item->started_at)) }} -
                            {{ date('h:i:s A', strtotime($item->finished_at)) }}
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection