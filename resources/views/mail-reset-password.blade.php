<div>
    <h2 class="text-xl font-semibold">Password changed</h2>
    <h3 class="text-lg font-medium mb-4">Web-based Validation Exam Quirino State College College of Information Technology and Computer Sciences</h3>
    <p>After log in, make sure to changed your password immediately. Thank you.</p>
    <p>
        Username: <b>{{ $admin->username }}</b> <br>
        New Password: <b>{{ $admin->password_default }}</b>
    </p>
</div>