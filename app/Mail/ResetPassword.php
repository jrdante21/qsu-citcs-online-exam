<?php

namespace App\Mail;

use App\Models\Admins;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $admin;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Admins $admin)
    {
        $this->admin = $admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Password Changed")->view('mail-reset-password', ['admin' => $this->admin]);
    }
}
