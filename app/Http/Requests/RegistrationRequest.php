<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\AlphaSpace;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => ['required', 'min:2', new AlphaSpace],
            'mname' => ['required', 'min:2', new AlphaSpace],
            'lname' => ['required', 'min:2', new AlphaSpace],
            'suffix' => ['nullable', 'min:2', new AlphaSpace],
            'gender' => ['required', 'numeric'],
            'bday' => ['required', 'before_or_equal:'.date('Y-01-01', strtotime('-14 years'))],
            'courses_id' => ['required', 'exists:courses,id'],
            'schools_none' => ['required', 'boolean'],
            'schools_id' => ['required_if:schools_none,false', 'exists:schools,id'],
            'schools_other' => ['required_if:schools_none,true'],
            'subjects' => ['required', 'array']
        ];
    }

    public function attributes()
    {
        return [
            'fname' => 'first name',
            'mname' => 'middle name',
            'lname' => 'last name',
            'bday' => 'birthdate'
        ];
    }

    public function messages()
    {
        return [
            'bday.before_or_equal' => "The birthdate must be 14 years below.",
            'schools_id.required_if' => "School is required",
            'schools_other.required_if' => "School name is required",
        ];
    }
}
