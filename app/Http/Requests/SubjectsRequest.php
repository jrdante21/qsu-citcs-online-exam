<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SubjectsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('subject');

        return [
            'title' => ['required', 'min:5', Rule::unique('courses_subjects', 'title')->ignore($id)],
            'courses_id' => ['required', 'exists:courses,id'],
            'questions_id' => ['nullable', 'exists:questions,id', Rule::unique('courses_subjects', 'questions_id')->ignore($id)]
        ];
    }
}
