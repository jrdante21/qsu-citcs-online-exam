<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Students;
use App\Models\StudentsSubjects;
use PDF;

class PDFController extends Controller
{
    public function __invoke($uuid, $type='')
    {
        switch ($type) {
            case 'student':
                $student = Students::with(['subjects','schools','courses'])->where('uuid', $uuid)->firstOrFail();
                $student->subjects = $student->subjects->makeHidden(['questions_items']);
                $pdf = PDF::loadView('pdfs.student', compact('student'));
                break;
            
            default:
                $subject = StudentsSubjects::with('students')->where('uuid', $uuid)->firstOrFail();
                $pdf = PDF::loadView('pdfs.exam', compact('subject'));
                break;
        }

        return $pdf->stream('document.pdf');
    }
}
