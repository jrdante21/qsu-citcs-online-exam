<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SpaController extends Controller
{
    public function __invoke()
    {
        $admin = auth()->guard('admin')->user();
        return view('admin', compact('admin'));
    }
}
