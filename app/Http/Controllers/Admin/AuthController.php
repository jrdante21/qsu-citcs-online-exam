<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $redirectTo = 'admin.panel';

    public function __construct()
    {
        $this->middleware('guest:admin,admin.panel')->except('logout');
    }

    public function index()
    {
        return view('admin');
    }

    public function login(Request $request)
    {
        $valid = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        if (auth()->guard('admin')->attempt($valid)) {
            return auth()->guard('admin')->user();

        } else {
            return response("Username and password is incorrect.", 422);
        }
    }

    public function logout()
    {
        auth()->guard('admin')->logout();
        return redirect()->route('admin.login');
    }
}
