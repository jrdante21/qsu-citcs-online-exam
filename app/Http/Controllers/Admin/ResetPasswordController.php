<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use App\Mail\ResetPassword;
use App\Models\Admins;

class ResetPasswordController extends Controller
{
    public function index()
    {
        return view('admin');
    }

    public function resetPassword(Request $request)
    {
        $valid = $request->validate([
            'email' => 'required|email|exists:admins,email'
        ]);
        
        // Transaction
        DB::beginTransaction();
        try {
            $password = Str::random(10);
            
            $admin = Admins::where('email',$valid['email'])->firstOrFail();
            $admin->password_default = $password;
            $admin->password = Hash::make($password);
            $admin->save();

            Mail::to($admin->email)->send(new ResetPassword($admin));
            DB::commit();
            return $valid;

        } catch (\Exception $e) {
            DB::rollback();
            return response("Something went wrong ".$e, 500);
        }
    }

    public function emailTemplate()
    {
        $admin = Admins::find(1);
        $admin->password_default = Str::random(10);
        return new ResetPassword($admin);
    }
}
