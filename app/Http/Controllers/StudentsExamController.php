<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Students;
use App\Models\StudentsSubjects;

class StudentsExamController extends Controller
{
    public function __construct()
    {
        $this->middleware('student-auth');
    }

    public function index()
    {
        $student = auth()->guard('student')->user()->id;
        $student = Students::with(['subjects','schools','courses'])->find($student);
        $student->subjects = $student->subjects->makeHidden(['questions_items','questions_title','questions_admin_id']);
        return view('app', compact('student'));
    }

    public function show(Request $request, $subject)
    {
        $student = auth()->guard('student')->user()->id;
        $subject = StudentsSubjects::where('uuid', $subject)->where('students_id', $student)->firstOrFail();

        if (empty($subject->finished_at)) {
            $subject->questions_items = collect($subject->questions_items)->map(function ($item) {
                $item['answer'] = null;
                return $item;
            });
        }

        return view('app', compact('subject'));
    }

    public function update(Request $request, $subject)
    {
        $student = auth()->guard('student')->user()->id;
        $subject = StudentsSubjects::where('uuid', $subject)->whereNull('finished_at')->where('students_id', $student)->firstOrFail();
        $valid = $request->validate([
            'date' => 'required|date',
            'answers' => 'array'
        ]);

        // Set time
        if (!empty($request->set_time)) {
            $dateStart = strtotime($valid['date']);
            $dateEnded = $dateStart + $subject->questions_time;
            if (!empty($subject->ended_at)) return $dateStart;
            
            $subject->started_at = date('Y-m-d H:i:s', $dateStart);
            $subject->ended_at = date('Y-m-d H:i:s', $dateEnded);
            $subject->save();
            return $dateStart;
        }

        // Set questions
        $results = collect($subject->questions_items)->map(function ($item, $key) use ($valid) {
            $item['answerOwn'] = (!empty($valid['answers'][$key])) ? $valid['answers'][$key] : null;
            return $item;
        });

        $subject->questions_items = $results;
        $subject->finished_at = date('Y-m-d H:i:s', strtotime($valid['date']));
        $subject->save();
        return $subject;
    }

    public function quit()
    {
        auth()->guard('student')->logout();
        return redirect()->route('registration');
    }
}
