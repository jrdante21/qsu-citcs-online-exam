<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use App\Http\Requests\RegistrationRequest;
use App\Models\Students;
use App\Models\Courses;
use App\Models\CoursesSubjects;

class RegistrationController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:student,students-exam');
    }

    public function index()
    {
        $courses = Courses::with(['subjects' => function($query){
            $query->selectRaw('id,courses_id,title');
            $query->where('published', 1);
        }])->get(['id','title','acronym']);

        $courses = collect($courses)->filter(function($value){
            return count($value->subjects) >= 1;
        });
        
        return view('app', compact('courses'));
    }

    public function store(RegistrationRequest $request)
    {
        $valid = $request->validated();
        $bday = date('Y-m-d', strtotime($valid['bday']));

        // Duplicated student information
        $duplicate = Students::where([
            ['fname', '=', $valid['fname']],
            ['mname', '=', $valid['mname']],
            ['lname', '=', $valid['lname']],
            ['bday', '=', $bday],
        ])->count();
        if ($duplicate >= 1) return response("Student information is duplicated, cannot be allowed.", 422);

        // Published subjects equal to subjects submited
        $subjects = CoursesSubjects::with('questions')->whereIn('id', $valid['subjects'])->where('published',1)->get();
        if (count($subjects) != count($valid['subjects'])) return response("Subjects are not alowed.", 422);

        // Set subjects and questionnaires
        $subjectsValid = [];
        foreach ($subjects as $s) {
            $uuid = Str::uuid();
            $subjectsValid[] = [
                'uuid' => $uuid,
                'subjects_id' => $s->id,
                'subjects_title' => $s->title,
                'questions_title' => $s->questions->title,
                'questions_items' => collect($s->questions->items)->shuffle()->all(),
                'questions_time' => $s->questions->time,
                'questions_admin_id' => $s->questions->admin_id,
            ];
        }

        // Transaction
        DB::beginTransaction();
        try {
            $student = new Students;
            $student->uuid = Str::uuid();
            $student->fname = $valid['fname'];
            $student->mname = $valid['mname'];
            $student->lname = $valid['lname'];
            if (!empty($valid['suffix'])) $student->suffix = $valid['suffix'];
            $student->gender = $valid['gender'];
            $student->bday = $bday;
            $student->courses_id = $valid['courses_id'];
            if (!$valid['schools_none']) {
                $student->schools_id = $valid['schools_id'];
            } else {
                $student->schools_other = $valid['schools_other'];
            }
            $student->save();
            $student->subjects()->createMany($subjectsValid);
            auth()->guard('student')->loginUsingId($student->id);
            
            DB::commit();
            return $student;

        } catch (\Exception $e) {
            DB::rollback();
            return response("Something went wrong ".$e, 500);
        }
    }
}
