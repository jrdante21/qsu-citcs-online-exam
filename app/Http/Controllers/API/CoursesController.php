<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Courses;
use App\Http\Requests\CoursesRequest;

class CoursesController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin-super')->except(['index','show']);
    }

    public function index(Request $request)
    {
        $courses = Courses::withSubjects(!empty($request->with_subjects))->get();
        return $courses;
    }

    public function show($course)
    {
        $course = Courses::with('subjects')->where('uuid', $course)->firstOrFail();
        return $course;
    }

    public function store(CoursesRequest $request)
    {
        $uuid = Str::uuid();
        $valid = $request->validated();

        $course = new Courses;
        $course->title = $valid['title'];
        $course->acronym = $valid['acronym'];
        $course->uuid = $uuid;
        $course->save();
        return $course;
    }

    public function update(CoursesRequest $request, Courses $course)
    {
        $valid = $request->validated();
        $course->title = $valid['title'];
        $course->acronym = $valid['acronym'];
        $course->save();
        return $course;
    }
}
