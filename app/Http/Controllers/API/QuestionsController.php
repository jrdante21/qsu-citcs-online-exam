<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\Questions;
use App\Http\Requests\QuestionsRequest;

class QuestionsController extends Controller
{
    public function index(Request $request)
    {
        $query = Questions::withSubject(empty($request->no_subjects))->orderByDesc('created_at');
        $query = (!empty($request->all)) ? $query->get() : $query->paginate(20);
        return $query;
    }

    public function show($question)
    {
        $query = Questions::where('uuid', $question)->firstOrFail();
        return $query;
    }

    public function store(QuestionsRequest $request)
    {   
        $uuid = Str::uuid();
        $valid = $request->validated();

        $query = new Questions;
        $query->uuid = $uuid;
        $query->title = $valid['title'];
        $query->admin_id = auth()->guard('admin')->user()->id;
        $query->save();
        return $uuid;
    }

    public function update(QuestionsRequest $request, $question)
    {
        $question = Questions::withSubject()->findOrFail($question);
        if ($question->admin_id != auth()->guard('admin')->user()->id) return response("Admin did not matched to the creator.",403);
        if (!empty($question->subject) && $question->subject->published) return response("Questionnaire can't be updated because it's already published.",422);

        $valid = $request->validated();
        $question->title = $valid['title'];
        $question->time = $valid['time'];
        $question->items = $valid['items'];
        $question->save();
        return $question->updated_at;
    }
}