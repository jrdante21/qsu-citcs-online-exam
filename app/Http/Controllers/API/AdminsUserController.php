<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\AdminsRequest;
use App\Rules\ConfirmPassword;

class AdminsUserController extends Controller
{
    public function changePassword(Request $request)
    {
        $valid = $request->validate([
            'password' => ['required', 'min:5', 'confirmed'],
            'password_old' => ['required', new ConfirmPassword],
        ]);

        $admin = auth()->guard('admin')->user();
        $admin->password = Hash::make($valid['password']);
        $admin->save();
        return $admin;
    }

    public function updateProfile(AdminsRequest $request)
    {
        $valid = $request->validated();
        
        $admin = auth()->guard('admin')->user();
        $admin->fname = $valid['fname'];
        $admin->mname = $valid['mname'];
        $admin->lname = $valid['lname'];
        $admin->email = $valid['email'];
        $admin->username = $valid['username'];
        $admin->save();
        return $admin;
    }
}
