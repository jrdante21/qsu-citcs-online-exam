<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Http\Requests\AdminsRequest;
use App\Models\Admins;

class AdminsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin-super');
    }

    public function index()
    {
        $query = Admins::where('is_super',0)->orderByDesc('created_at')->get();
        return $query;
    }

    public function store(AdminsRequest $request)
    {
        $valid = $request->validated();
        $admin = new Admins;
        $admin->fname = $valid['fname'];
        $admin->mname = $valid['mname'];
        $admin->lname = $valid['lname'];
        $admin->email = $valid['email'];
        $admin->username = $valid['username'];
        $admin->password = Hash::make($valid['password']);
        $admin->is_super = 0;
        $admin->save();
        return $admin;
    }

    public function update(AdminsRequest $request, Admins $admin)
    {
        $valid = $request->validated();
        $admin->fname = $valid['fname'];
        $admin->mname = $valid['mname'];
        $admin->lname = $valid['lname'];
        $admin->email = $valid['email'];
        $admin->username = $valid['username'];
        if (!empty($valid['password'])) $admin->password = Hash::make($valid['password']);
        $admin->is_super = 0;
        $admin->save();
        return $admin;
    }
}
