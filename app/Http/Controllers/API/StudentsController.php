<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Students;

class StudentsController extends Controller
{
    public function index(Request $request)
    {
        $students = Students::with('courses')->searchName($request->search)->orderByDesc('created_at');
        if (!empty($request->course)) $students->where('courses_id', $request->course);

        $students = $students->paginate(20);
        return $students;
    }
    public function show($student)
    {
        $student = Students::with(['courses:id,title','subjects','schools'])->where('uuid',$student)->firstOrFail();
        return $student;
    }
}
