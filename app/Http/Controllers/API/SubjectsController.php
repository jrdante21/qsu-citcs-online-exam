<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Models\CoursesSubjects;
use App\Models\StudentsSubjects;
use App\Http\Requests\SubjectsRequest;

class SubjectsController extends Controller
{
    public function show($subject)
    {
        $subject = CoursesSubjects::with(['admin','questions'])->where('uuid', $subject)->firstOrFail();
        $subject->students = StudentsSubjects::with('students')->where('subjects_id', $subject->id)->orderByDesc('created_at')->paginate(20);
        return $subject;
    }

    public function store(SubjectsRequest $request)
    {
        $uuid = Str::uuid();
        $valid = $request->validated();

        $subject = new CoursesSubjects;
        $subject->title = $valid['title'];
        $subject->courses_id = $valid['courses_id'];
        $subject->admin_id = auth()->guard('admin')->user()->id;
        $subject->uuid = $uuid;
        $subject->published = 0;
        $subject->save();
        return $subject;
    }

    public function update(SubjectsRequest $request, CoursesSubjects $subject)
    {
        if (!empty($subject->published)) return response("Subject can't be edited if it's published.", 422);
        if ($subject->admin_id != auth()->guard('admin')->user()->id) return response("Admin did not matched to the creator.",403);

        $valid = $request->validated();
        $subject->title = $valid['title'];
        $subject->questions_id = $valid['questions_id'];
        $subject->save();
        return $subject;
    }

    public function publish($subject)
    {
        $subject = CoursesSubjects::with('questions')->findOrFail($subject);
        $admin = auth()->guard('admin')->user();
        if (!$admin->is_super && $subject->admin_id != $admin->id) return response("Admin did not matched to the creator.",403);

        // Unpublish
        if ($subject->published) {
            $subject->published = 0;
            $subject->save();
            return $subject;
        }

        if (empty($subject->questions)) return response("Subject must have a questionnaire.", 422);
        if ($subject->questions->progress['percent'] < 100) return response("Subject's questionnaire must be %100 complete.", 422);

        $subject->published = 1;
        $subject->save();
        return $subject;
    }
}
