<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Students;

class ExamTimelineController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $query = Students::selectRaw("
            DATE(created_at) as date,
            GROUP_CONCAT(id) as ids
        ")
        ->groupBy('date')
        ->orderByDesc('date');
        if (!empty($request->course)) $query->where('courses_id', $request->course);

        $query = $query->simplePaginate(10);
        foreach ($query as $q) {
            $ids = explode(',', $q->ids);
            $q->students = Students::with(['courses:id,title','subjects'])->whereIn('id', $ids)->get();
        }

        return $query;
    }
}
