<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Schools;

class SchoolsController extends Controller
{
    public function admin(Request $request)
    {
        $query = Schools::withCount('students');
        if (!empty($request->search)) $query->where('name', 'like', '%'.$request->search.'%');

        $query = $query->paginate(20);
        return $query;
    }

    public function public(Request $request)
    {
        $query = Schools::where('name', 'like', '%'.$request->search.'%')->limit(20)->get();
        return $query;
    }
}
