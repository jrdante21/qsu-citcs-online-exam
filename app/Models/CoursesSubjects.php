<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoursesSubjects extends Model
{
    use HasFactory;

    public function getPublishedAttribute($value)
    {
        return (empty($value)) ? false : true;
    }

    public function admin()
    {
        return $this->belongsTo(Admins::class);
    }

    public function questions()
    {
        return $this->belongsTo(Questions::class);
    }
}
