<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentsSubjects extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $casts = ['questions_items' => 'array'];
    protected $appends = ['items_count', 'time_str', 'time_left', 'results'];

    public function getItemsCountAttribute()
    {
        return count($this->questions_items);
    }

    public function getTimeStrAttribute()
    {
        return (empty($this->questions_time)) ? null : gmdate('H:i:s', $this->questions_time);
    }

    public function getTimeLeftAttribute()
    {
        if (empty($this->ended_at)) return $this->questions_time;
        $timeEnded = strtotime($this->ended_at);
        $timeFinished = (empty($this->finished_at)) ? time() : strtotime($this->finished_at);
        return max(0, ($timeEnded - $timeFinished));
    }

    public function getResultsAttribute()
    {
        $missed=0; $correct=0; $wrong=0;
        foreach ($this->questions_items as $q) {
            if (empty($q['answerOwn'])) {
                $missed += 1;
            } elseif ($q['answer'] == $q['answerOwn']) {
                $correct += 1;
            } else {
                $wrong += 1;
            }
        }
        $percent = floor(($correct / count($this->questions_items)) * 100);
        return compact('missed', 'correct', 'wrong', 'percent');
    }

    public function students()
    {
        return $this->belongsTo(Students::class);
    }
}
