<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class Admins extends Authenticatable
{
    use HasFactory, Notifiable;
    
    protected $hidden = ['password'];
    protected $appends = ['fullname','completename','is_password_default'];

    public function getIsSuperAttribute($value)
    {
        return empty($value) ? false : true;
    }

    public function getFullnameAttribute()
    {
        return "{$this->fname} {$this->lname}";
    }

    public function getCompletenameAttribute()
    {
        return "{$this->fname} {$this->mname} {$this->lname}";
    }

    public function getIsPasswordDefaultAttribute()
    {
        return Hash::check($this->password_default, $this->password);
    }
}
