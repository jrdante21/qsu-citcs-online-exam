<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Students extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $appends = ['fullname','gender_str','age'];

    public function getFullnameAttribute()
    {
        $name = "{$this->fname} {$this->mname} {$this->lname} {$this->suffix}";
        return trim($name);
    }

    public function getGenderStrAttribute()
    {
        return ($this->gender <= 1) ? 'Male' : 'Female';
    }

    public function getAgeAttribute () {
        $diff = date_diff(date_create("now"), date_create($this->bday));
        return $diff->y;
    }

    public function subjects()
    {
        return $this->hasMany(StudentsSubjects::class);
    }

    public function schools()
    {
        return $this->belongsTo(Schools::class)->withDefault(function($schools, $student){
            $schools->name = $student->schools_other;
        });
    }

    public function courses()
    {
        return $this->belongsTo(Courses::class);
    }

    public function scopeSearchName($query, $search='')
    {
        if (empty($search)) return $query;

        $search = '%'.$search.'%';
        return $query->whereRaw("
            CONCAT(fname, ' ', mname, ' ', lname) LIKE ? OR
            CONCAT(fname, ' ', lname) LIKE ? OR
            CONCAT(lname, ' ', fname) LIKE ?
        ", [$search, $search, $search]);
    }
}
