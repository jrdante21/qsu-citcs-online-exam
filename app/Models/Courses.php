<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    use HasFactory;

    public function subjects()
    {
        return $this->hasMany(CoursesSubjects::class)->with(['questions' => function($query){
            $query->without('admin');
        }]);
    }

    public function students()
    {
        return $this->hasMany(Students::class);
    }

    public function scopeWithSubjects($query, $yes=true)
    {
        return ($yes) ? $query->with('subjects') : $query;
    }
}

