<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Questions extends Model
{
    use HasFactory;
    protected $with = ['admin'];
    protected $appends = ['time_str', 'progress'];
    protected $casts = [ 'items' => 'array' ];

    public function getItemsAttribute($value)
    {
        return (empty($value)) ? [] : json_decode($value, true);
    }

    public function getTimeStrAttribute()
    {
        return (empty($this->time)) ? null : gmdate('H:i:s', $this->time);
    }

    public function getProgressAttribute()
    {
        $count = count($this->items);
        $answered = 0; $questions = 0; $options = 0;
        foreach ($this->items as $i) {
            if (!empty($i['answer'])) $answered += 1;
            if (!empty($i['question'])) $questions += 1;

            $opt = 0;
            foreach ($i['options'] as $o) {
                if (!empty($o['text'])) $opt += 1;
            }
            if ($opt >= 4) $options += 1;
        }

        $percent = $count + (($this->time >= 60) ? 1 : 0);
        $percent = min(floor(($percent / 11) * 100), 100);
        
        $minCount = max(10, $count);
        $details = compact('answered', 'questions', 'options');
        if ($count >= 1) {
            foreach ($details as $d) {
                $percent += floor(($d / $minCount) * 100);
            }
        }
        $percent = floor($percent / 4);

        if ($percent >= 100) {
            $status = "default";
        
        } elseif ($percent < 100 && $percent >= 70) {
            $status = "success";

        } elseif ($percent < 70 && $percent >= 40) {
            $status = "warning";

        } else {
            $status = "error";
        }
        
        $details['count'] =  $count;
        return compact('percent', 'status', 'details');
    }

    public function admin()
    {
        return $this->belongsTo(Admins::class);
    }

    public function subject()
    {
        return $this->hasOne(CoursesSubjects::class);
    }

    public function scopeWithSubject($query, $yes=true)
    {
        return (!$yes) ? $query->doesntHave('subject') : $query->with('subject');
    }
}
